# Calendar Links Tokens

Introduction
------------

This module provides a dynamic token to generate calendar invite links for 
Google, Yahoo, Outlook calendars + a .ics file link.

You can use this token anywhere, a very useful use case is using the token 
into the email message of event registration webforms.

Requirements
------------

* spatie/calendar-links (https://github.com/spatie/calendar-links) library.

Installation
------------

Install as usual, see 
https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-
modules-find-import-enable-configure-drupal-8 for further information.

Usage
-----

Token format:`[calendar_links:parameters:nid|date_start_field|
date_end_field|title_field|description_field|location_field]`

Example usage using only fields: `[calendar_links:parameters:5581|
field_start|field_end|title|body|field_venue]`

Example usage using only string values: `[calendar_links:parameters:5581|
Wednesday 14th October 2020 9:00pm|Wednesday 14th October 2020 10:30pm|
Event A title|Details|Example st 12]`

Example usage using a mix of string values and fields: `[calendar_links:
parameters:5581|field_start|field_end|title|body|Test st 12]`

Maintainers
-----------

This module is maintained by developers at Morpht. For more information 
on the company and our offerings, see https://morpht.com
